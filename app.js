const puppeteer = require('puppeteer');

const url = 'https://udemy.com';


(async () => {
    const browser = await puppeteer.launch();
    const page = await browser.newPage();
    await page.setViewport({
        width: 1920,
        height: 1080,
        deviceScaleFactor: 1,
      });
    await page.goto(url, {waitUntil: 'networkidle2'});
    await page.waitForSelector('nav');
    await page.hover('nav');
    await page.click('.header--dropdown-button-text--2jtIM');
    const link = await page.$eval('ul.list-menu--section--BZ3j9 > li > a', elem => elem.attributes.href.value);
    await page.goto(url + link, {waitUntil: 'networkidle2'});
    await page.waitForSelector('.main-content-wrapper');
        
    for(let i = 0; i < 5; i++) {
        let list = [];
        const attributes = {
            courseTitle: await page.$eval(`div[data-index="${i}"] > div > div > a > div > div.course-card--main-content--3xEIw > div.course-card--course-title--2f7tE`, elem => elem.innerText),
            courseInstructor: await page.$eval(`div[data-index="${i}"] > div > div > a > div > div.course-card--main-content--3xEIw > div.course-card--instructor-list--lIA4f`, elem => elem.innerText),
            rating: await page.$eval(`div[data-index="${i}"] > div > div > a > div > div.course-card--main-content--3xEIw > div.course-card--row--1OMjg > span.star-rating--star-wrapper--2eczq > span[data-purpose="rating-number"]`, elem => elem.innerText),
            reviews: await page.$eval(`div[data-index="${i}"] > div > div > a > div > div.course-card--main-content--3xEIw > div.course-card--row--1OMjg > span.course-card--reviews-text--12UpL`, elem => elem.innerText),
            price: await page.$eval(`div[data-index="${i}"] > div > div > a > div > div.course-card--main-content--3xEIw > div[data-purpose="price-text-container"] > div[data-purpose="course-price-text"] > span + span`, elem => elem.innerText)
    };
        list.push(attributes);
        console.log(list);
    }
    await browser.close();
}) ();